# Interview Task

* Start application with `mvn clean spring-boot:run`
* Swagger UI is on http://localhost:8080/swagger-ui/index.html

### Q1: please add missing indexes/constraints/datatypes for DB schema above if needed
```
CREATE TABLE SCHEMA1.USERS
(
    ID_ UNIQUEIDENTIFIER PRIMARY KEY    --- Unique user id
    LOGIN_NAME_ VARCHAR2(255)           --- user login name
    FIRST_NAME_ VARCHAR2(50)           --- user first name
    LAST_NAME_ VARCHAR2(50)            --- user last name
}

CREATE TABLE SCHEMA2.TASKS
(
    ID_ UNIQUEIDENTIFIER PRIMARY KEY    --- unique task id
    USER_NOTE_ VARCHAR2(255)            --- some users notes
    TASK_DATA_ VARCHAR2(max_size BYTE)  --- some task data
    ACQUIRED_BY_ UNIQUEIDENTIFIER       --- user who acquires this task
    CREATED_BY_ UNIQUEIDENTIFIER        --- user who creates this task
}

CREATE INDEX IDX_ACQUIRED_BY_
ON TASKS (ACQUIRED_BY_);

CREATE INDEX IDX_USER_NOTE_
ON TASKS (USER_NOTE_);
```

### Q2: Implement microservices that does the following:

* supports REST endpoins to create and update tasks
* supports REST endpoins to retrive tasks with filtering by:
  * ACQUIRED_BY_
  * USER_NOTE_


### Q3: implement several unit tests for developed microservices

### Q4: what clarifying questions should be asked ( just ask and answer by yourself)
* What database system is used
  * H2, but any JPA supported database will be fine
* What can be LOGIN_NAME_
  * since email could be used for login, I used the VARCHAR2(255)
* Do we need to connect USERS table and TASKS table via FOREIGN KEY?
  * no, since the Q2 specifies microservice for tasks only
  * if the task was to create microservices for both USERS and TASKS then I'd use FOREIGN KEY on both ACQUIRED_BY_ and CREATED_BY_
* What can be in TASK_DATA_
  * TASK_DATA_ can contain sample code and potentially very long descriptions
