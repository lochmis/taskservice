package test.taskService.exceptions;

import java.util.UUID;

public class TaskAlreadyExistsException extends RuntimeException {

    public TaskAlreadyExistsException(UUID id) {
        super("Task with id " + id + " already exists.");
    }
}
