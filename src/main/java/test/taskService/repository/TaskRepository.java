package test.taskService.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import test.taskService.model.Task;

import java.util.List;
import java.util.Set;
import java.util.UUID;

public interface TaskRepository extends JpaRepository<Task, UUID> {

    @Query("SELECT t FROM Task t WHERE t.acquiredBy IN ?1"
            + " AND t.userNote IN ?2")
    List<Task> findFilteredByAcquiredIdsAndUserNotes(Set<UUID> acquiredByIds, Set<String> userNotes);

    @Query("SELECT t FROM Task t WHERE t.acquiredBy IN ?1")
    List<Task> findFilteredByAcquiredIds(Set<UUID> acquiredByIds);

    @Query("SELECT t FROM Task t WHERE t.userNote IN ?1")
    List<Task> findFilteredByUserNotes(Set<String> userNotes);
}
