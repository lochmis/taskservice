package test.taskService.controller;

import io.swagger.v3.oas.annotations.Operation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import test.taskService.exceptions.TaskAlreadyExistsException;
import test.taskService.model.TaskFilter;
import test.taskService.repository.TaskRepository;
import test.taskService.model.Task;
import test.taskService.exceptions.TaskNotFoundException;

import java.util.List;
import java.util.UUID;

@RestController
public class TaskController {

    private final TaskRepository repository;

    public TaskController(TaskRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/tasks/{id}")
    @Operation(summary = "Get Task by Id",
            description = "Retrieves Task with provided ID, or returns Exception if ID does not exist.")
    Task getById(@PathVariable UUID id) {
        return repository.findById(id)
                .orElseThrow(() -> new TaskNotFoundException(id));
    }

    @PostMapping("/tasks/filtered")
    @Operation(summary = "Get filtered Tasks",
            description = "Retrieves Tasks filtered by acquiredBy and/or userNote.")
    List<Task> findTasks(@RequestBody TaskFilter filter) {
        if (!filter.getAcquiredByIds().isEmpty() && !filter.getUserNotes().isEmpty()) {
            return repository.findFilteredByAcquiredIdsAndUserNotes(filter.getAcquiredByIds(), filter.getUserNotes());
        } else if (!filter.getAcquiredByIds().isEmpty()) {
            return repository.findFilteredByAcquiredIds(filter.getAcquiredByIds());
        } else if (!filter.getUserNotes().isEmpty()) {
            return repository.findFilteredByUserNotes(filter.getUserNotes());
        } else {
            return repository.findAll();
        }
    }

    @PostMapping("/tasks")
    @Operation(summary = "Create new Task",
            description = "Creates new task, if ID is not provided, it will be automatically generated")
    Task createTask(@RequestBody Task newTask) {
        if (newTask.getId() == null) {
            newTask.setId(UUID.randomUUID());
        } else if (repository.findById(newTask.getId()).isPresent()) {
            throw new TaskAlreadyExistsException(newTask.getId());
        }
        return repository.save(newTask);
    }

    @PutMapping("/tasks/{id}")
    @Operation(summary = "Update Task", description = "Updates (replaces) task")
    Task updateTask(@RequestBody Task newTask, @PathVariable UUID id) {
        return repository.findById(id)
                .map(task -> {
                    task.setUserNote(newTask.getUserNote());
                    task.setTaskData(newTask.getTaskData());
                    task.setAcquiredBy(newTask.getAcquiredBy());
                    task.setCreatedBy(newTask.getCreatedBy());
                    return repository.save(task);
                })
                .orElseGet(() -> {
                    newTask.setId(id);
                    return repository.save(newTask);
                });
    }
}
