package test.taskService.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "TASKS", indexes = {
        @Index(name = "IDX_ACQUIRED_BY_", columnList = "ACQUIRED_BY_"),
        @Index(name = "IDX_USER_NOTE_", columnList = "USER_NOTE_")
})
public class Task implements Serializable {

    @Schema(required = false, description = "Unique task id")
    @Id
    @Column(name = "ID_", columnDefinition = "uuid")
    private UUID id;

    @Schema(required = false, description = "Some users notes")
    @Column(name = "USER_NOTE_")
    private String userNote;

    @Schema(required = false, description = "Some task data")
    @Column(name = "TASK_DATA_")
    private String taskData;

    @Schema(required = false, description = "User who acquires this task")
    @Column(name = "ACQUIRED_BY_", columnDefinition = "uuid")
    private UUID acquiredBy;

    @Schema(required = false, description = "User who creates this task")
    @Column(name = "CREATED_BY_", columnDefinition = "uuid")
    private UUID createdBy;
}
