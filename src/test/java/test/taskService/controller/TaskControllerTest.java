package test.taskService.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import test.taskService.exceptions.TaskAlreadyExistsException;
import test.taskService.exceptions.TaskNotFoundException;
import test.taskService.model.Task;
import test.taskService.model.TaskFilter;
import test.taskService.repository.TaskRepository;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class TaskControllerTest {

    @Autowired
    private TaskRepository repository;

    @Autowired
    private TaskController controller;

    private final UUID userId1 = UUID.randomUUID();
    private final UUID userId2 = UUID.randomUUID();
    private final UUID userId3 = UUID.randomUUID();
    private final UUID missingUserId = UUID.randomUUID();
    private final UUID taskId1 = UUID.randomUUID();
    private final UUID taskId2 = UUID.randomUUID();
    private final UUID taskId3 = UUID.randomUUID();
    private final UUID taskId4 = UUID.randomUUID();
    private final UUID missingTaskId = UUID.randomUUID();

    private final Task task1 = Task.builder()
            .id(taskId1)
            .userNote("completed")
            .createdBy(userId1)
            .acquiredBy(userId2)
            .build();
    private final Task task2 = Task.builder()
            .id(taskId2)
            .userNote("in progress")
            .createdBy(userId1)
            .acquiredBy(userId2)
            .build();
    private final Task task3 = Task.builder()
            .id(taskId3)
            .userNote("completed")
            .createdBy(userId1)
            .acquiredBy(userId3)
            .build();
    private final Task task4 = Task.builder()
            .id(taskId4)
            .userNote("in progress")
            .createdBy(userId1)
            .acquiredBy(userId3)
            .build();

    @BeforeEach
    void setup() {
        repository.deleteAll();
        repository.save(task1);
        repository.save(task2);
        repository.save(task3);
        repository.save(task4);
    }

    @Test
    void whenRequestingTaskByIdThenGetCorrectTask() {
        Task actual = controller.getById(taskId1);
        assertEquals(actual, task1);
    }

    @Test
    void whenRequestingNonExistentTaskIdThenReceive404Error() {
        TaskNotFoundException thrown = assertThrows(TaskNotFoundException.class, () -> {
            controller.getById(missingTaskId);
        }, "TaskNotFoundException was expected");
        assertEquals("Could not find Task " + missingTaskId, thrown.getMessage());
    }

    @Test
    void whenRequestingInvalidTaskIdThenReceiveNPEException() {
        InvalidDataAccessApiUsageException thrown = assertThrows(InvalidDataAccessApiUsageException.class, () -> {
            controller.getById(null);
        }, "InvalidDataAccessApiUsageException was expected");
        assertTrue(thrown.getMessage().contains("The given id must not be null!"));
    }

    @Test
    void whenCreatingTaskThenCorrectTaskIsStored() {
        UUID taskId = UUID.randomUUID();
        Task expected = Task.builder()
                .id(taskId)
                .taskData("completed")
                .createdBy(userId1)
                .acquiredBy(userId3)
                .build();
        controller.createTask(expected);
        Optional<Task> actual = repository.findById(taskId);
        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }

    @Test
    void whenCreatingTaskWithExistingIDThenErrorIsThrown() {
        Task expected = Task.builder()
                .id(taskId1)
                .taskData("completed")
                .createdBy(userId1)
                .acquiredBy(userId3)
                .build();
        TaskAlreadyExistsException thrown = assertThrows(TaskAlreadyExistsException.class, () -> {
            controller.createTask(expected);
        }, "TaskAlreadyExistsException was expected");
        assertEquals("Task with id " + taskId1 + " already exists.", thrown.getMessage());
    }

    @Test
    void whenUpdatingExistingTaskThenTaskGetsUpdated() {
        Task expected = Task.builder()
                .id(taskId2)
                .taskData("completed")
                .createdBy(userId2)
                .acquiredBy(userId3)
                .build();
        Task actual = controller.getById(taskId2);
        assertNotEquals(expected, actual);
        controller.updateTask(expected, taskId2);
        actual = controller.getById(taskId2);
        assertEquals(expected, actual);
    }

    @Test
    void whenUpdatingNonExistingTaskThenTaskGetsCreated() {
        Task expected = Task.builder()
                .id(missingTaskId)
                .taskData("completed")
                .createdBy(userId2)
                .acquiredBy(userId3)
                .build();
        TaskNotFoundException thrown = assertThrows(TaskNotFoundException.class, () -> {
            controller.getById(missingTaskId);
        }, "TaskNotFoundException was expected");
        assertEquals("Could not find Task " + missingTaskId, thrown.getMessage());
        controller.updateTask(expected, missingTaskId);
        Task actual = controller.getById(missingTaskId);
        assertEquals(expected, actual);
    }

    @Test
    void whenFindingTasksByOneAcquiredByIdThenGetAllValidTasks() {
        TaskFilter filter = TaskFilter.builder()
                .acquiredByIds(Collections.singleton(userId2))
                .userNotes(Collections.EMPTY_SET)
                .build();
        List<Task> tasks = controller.findTasks(filter);
        assertEquals(2, tasks.size());
        assertTrue(tasks.contains(task1));
        assertTrue(tasks.contains(task2));
    }

    @Test
    void whenFindingTasksByOneAcquiredByIdAndOneUserNoteThenGetAllValidTasks() {
        TaskFilter filter = TaskFilter.builder()
                .acquiredByIds(Collections.singleton(userId2))
                .userNotes(Collections.singleton("completed"))
                .build();
        List<Task> tasks = controller.findTasks(filter);
        assertEquals(1, tasks.size());
        assertTrue(tasks.contains(task1));
    }

    @Test
    void whenFindingTasksByMultipleAcquiredByIdsThenGetAllValidTasks() {
        TaskFilter filter = TaskFilter.builder()
                .acquiredByIds(new HashSet<>(Arrays.asList(userId2, userId3)))
                .userNotes(Collections.EMPTY_SET)
                .build();
        List<Task> tasks = controller.findTasks(filter);
        assertEquals(4, tasks.size());
        assertTrue(tasks.contains(task1));
        assertTrue(tasks.contains(task2));
        assertTrue(tasks.contains(task3));
        assertTrue(tasks.contains(task4));
    }

    @Test
    void whenFindingTasksByMultipleAcquiredByIdAndOneUserNoteThenGetAllValidTasks() {
        TaskFilter filter = TaskFilter.builder()
                .acquiredByIds(new HashSet<>(Arrays.asList(userId2, userId3)))
                .userNotes(Collections.singleton("completed"))
                .build();
        List<Task> tasks = controller.findTasks(filter);
        assertEquals(2, tasks.size());
        assertTrue(tasks.contains(task1));
        assertTrue(tasks.contains(task3));
    }

    @Test
    void whenFindingTasksByMultipleAcquiredByIdAndMultipleUserNoteThenGetAllValidTasks() {
        TaskFilter filter = TaskFilter.builder()
                .acquiredByIds(new HashSet<>(Arrays.asList(userId2, userId3)))
                .userNotes(new HashSet<>(Arrays.asList("completed", "in progress")))
                .build();
        List<Task> tasks = controller.findTasks(filter);
        assertEquals(4, tasks.size());
        assertTrue(tasks.contains(task1));
        assertTrue(tasks.contains(task2));
        assertTrue(tasks.contains(task3));
        assertTrue(tasks.contains(task4));
    }

    @Test
    void whenFindingTasksByOneAcquiredByIdAndMultipleUserNoteThenGetAllValidTasks() {
        TaskFilter filter = TaskFilter.builder()
                .acquiredByIds(new HashSet<>(Collections.singletonList(userId2)))
                .userNotes(new HashSet<>(Arrays.asList("completed", "in progress")))
                .build();
        List<Task> tasks = controller.findTasks(filter);
        assertEquals(2, tasks.size());
        assertTrue(tasks.contains(task1));
        assertTrue(tasks.contains(task2));
    }

    @Test
    void whenFindingTasksByOneUserNoteThenGetAllValidTasks() {
        TaskFilter filter = TaskFilter.builder()
                .acquiredByIds(Collections.EMPTY_SET)
                .userNotes(new HashSet<>(Arrays.asList("in progress")))
                .build();
        List<Task> tasks = controller.findTasks(filter);
        assertEquals(2, tasks.size());
        assertTrue(tasks.contains(task2));
        assertTrue(tasks.contains(task4));
    }

    @Test
    void whenFindingTasksByMultipleUserNoteThenGetAllValidTasks() {
        TaskFilter filter = TaskFilter.builder()
                .acquiredByIds(Collections.EMPTY_SET)
                .userNotes(new HashSet<>(Arrays.asList("completed", "in progress")))
                .build();
        List<Task> tasks = controller.findTasks(filter);
        assertEquals(4, tasks.size());
        assertTrue(tasks.contains(task1));
        assertTrue(tasks.contains(task2));
        assertTrue(tasks.contains(task3));
        assertTrue(tasks.contains(task4));
    }

    @Test
    void whenFindingTasksByOneAcquiredByIdThenGetTasks() {
        TaskFilter filter = TaskFilter.builder()
                .acquiredByIds(new HashSet<>(Collections.singletonList(missingUserId)))
                .userNotes(Collections.EMPTY_SET)
                .build();
        List<Task> tasks = controller.findTasks(filter);
        assertEquals(0, tasks.size());
    }

    @Test
    void whenTryingSQLInjectionInCreationThenTaskIsStoredNormally() {
        UUID taskId = UUID.randomUUID();
        Task expected = Task.builder()
                .id(taskId)
                .taskData("completed'); DROP TABLE TASKS; --")
                .createdBy(userId1)
                .acquiredBy(userId3)
                .build();
        controller.createTask(expected);
        Optional<Task> actual = repository.findById(taskId);
        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }

    @Test
    void whenTryingSQLInjectionInFilterThenTableIsNotDeleted() {
        TaskFilter filter = TaskFilter.builder()
                .acquiredByIds(Collections.EMPTY_SET)
                .userNotes(Collections.singleton("completed'); DROP TABLE TASKS; --"))
                .build();
        List<Task> tasks = controller.findTasks(filter);
        assertEquals(0, tasks.size());
        filter = TaskFilter.builder()
                .acquiredByIds(Collections.EMPTY_SET)
                .userNotes(new HashSet<>(Arrays.asList("completed", "in progress")))
                .build();
        tasks = controller.findTasks(filter);
        assertEquals(4, tasks.size());
        assertTrue(tasks.contains(task1));
        assertTrue(tasks.contains(task2));
        assertTrue(tasks.contains(task3));
        assertTrue(tasks.contains(task4));
    }
}